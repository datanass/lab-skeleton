require("dotenv").config();
const util = require("util");
const uuid = require("uuid");
const mysql = require("mysql2-promise")();
const logger = require("./logger");

const init = async () => {
  mysql.configure({
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT,
    database: process.env.RDS_DATABASE,
  });
  //  try {
  //    await connect();
    //  console.log("Database connected!");
  //  } catch (e) {
  //    console.error(e);
  //  }
};

const queryProductById = async (productId) => {
  return (await mysql.query(`SELECT *
                              FROM products
                              WHERE id = "${productId}";`))[0][0];
};

const queryRandomProduct = async () => {
  ///TODO: Implement this
  const randomProduct = await mysql.query("SELECT * FROM products ORDER BY RAND() LIMIT 1;");
  return randomProduct[0][0];
};

const queryAllProducts = async () => {
  ///TODO: Implement this
  return (await mysql.query("SELECT * FROM products;"))[0];

};

const queryAllCategories = async () => {
  return (await mysql.query("SELECT * FROM categories;"))[0];
};

const queryAllOrders = async () => {
  return (await mysql.query("SELECT * FROM orders;"))[0];
};

const queryOrdersByUser = async (userId) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                                    INNER JOIN order_items ON orders.id = order_items.order_id
                           WHERE user_id = "${userId}"`)
  )[0]; // Not a perfect analog for NoSQL, since SQL cannot return a list.
};

const queryOrderById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                           WHERE id = "${id}"`)
  )[0][0];
};

const queryUserById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM users
                           WHERE id = "${id}";`)
  )[0][0];
};

const queryAllUsers = async () => {
  return (await mysql.query("SELECT * FROM users"))[0];
};

const insertOrder = async (order) => {  ///TODO: Implement this

  const orderId = uuid.v4();
  let orderItems = [];
  for (let prod of Object.values(order.products)){
    orderItems.push(`("${uuid.v4()}", "${orderId}", "${prod.product_id}", ${prod.quantity})`)
  }
  let values = orderItems.join(", ")

  // console.log(orderItems)
  // console.log(orderId)
  logger.info(orderItems)
  logger.info(orderId)

  await mysql.query(`INSERT INTO orders (id, user_id, total_amount) VALUES ("${orderId}", "${order.user_id}", ${order.total_amount})`)
  
  if (orderItems.length){
    await mysql.query(`INSERT INTO order_items (id, order_id, product_id, quantity) VALUES ${values}`)
  }
  // Select new order and its items based on the order id 
  const newOrder = (await mysql.query(`SELECT id, user_id, total_amount FROM orders WHERE id = "${orderId}"`))[0][0];
  const newOrderItems = (await mysql.query(`SELECT product_id, quantity FROM order_items WHERE order_id = "${orderId}"`))[0];
  // console.log("newOrder", newOrder)
  // console.log("newOrderItems", newOrderItems)
  logger.info("newOrder", newOrder)
  logger.info("newOrderItems", newOrderItems)


  return {
    id: newOrder.id,
    user_id: newOrder.user_id,
    total_amount: newOrder.total_amount,
    products: newOrderItems.map(product => ({
      product_id: product[0],
      quantity: product[1]
    }))
  }
};

const updateUser = async (id, updates) => {  ///TODO: Implement this

  // use this array to build the list of arguments to find the user
  let args = []

  const { name, email, password } = updates;
  if (name){
    args.push(`name = "${updates.name}"`);
  } 
  if (email){
    args.push(`email = "${updates.email}"`);
  }
  if (password){
    args.push(`password = "${updates.password}"`);
  }
  
  // write the query
  let sqlQuery = "UPDATE users SET " 
  sqlQuery += args.join(", ") 
  sqlQuery += ` WHERE id = "${id}";`;
  // update the user
  await mysql.query(sqlQuery)
  const newuser = await queryUserById(id)
  return newuser;

};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
