require("dotenv").config();
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const {
  GetCommand,
  ScanCommand,
  PutCommand,
  UpdateCommand,
  DynamoDBDocumentClient,
} = require("@aws-sdk/lib-dynamodb");
const uuid = require("uuid");

let client;
let docClient;

const init = () => {
  client = new DynamoDBClient({ region: process.env.AWS_REGION });
  docClient = DynamoDBDocumentClient.from(client);
  console.log("DynamoDB connected!");
};

const queryRandomProduct = async () => {
  ///TODO: IMPLEMENT THIS

  const command = new ScanCommand({
    TableName: "Products",
  });
  const response = await docClient.send(command);
  const randomIndex = Math.floor(Math.random() *  response.Items.length);
  return response.Items[randomIndex];
  

};

const queryProductById = async (productId) => {
  const command = new GetCommand({
    TableName: "Products",
    Key: {
      id: productId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllProducts = async (category = "") => {  ///TODO: Implement this
  
  if (category === ""){ // return all products if no category specified.
    const command = new ScanCommand({ TableName: "Products" });
    const response = await docClient.send(command);
    return response.Items;
  }
  else{ // return all products from specified category
    const command = new ScanCommand({
      TableName: "Products",
      FilterExpression: "category_id = :category",
      ExpressionAttributeValues: {
        ":category": category,
      },
    });
    const response = await docClient.send(command);

    return response.Items;
  }

};

const queryAllCategories = async () => {
  const command = new ScanCommand({
    TableName: "Categories",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllOrders = async () => {
  const command = new ScanCommand({
    TableName: "Orders",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrdersByUser = async (userId) => {
  const command = new ScanCommand({
    TableName: "Orders",
    FilterExpression: "user_id = :user_id",
    ExpressionAttributeValues: {
      ":user_id": userId,
    },
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrderById = async (userId) => {
  const command = new GetCommand({
    TableName: "Orders",
    Key: {
      id: userId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryUserById = async (userId) => {
  const command = new GetCommand({
    TableName: "Users",
    Key: {
      id: userId,
    },
    ProjectionExpression: "id, email",
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllUsers = async () => {
  const command = new ScanCommand({
    TableName: "Users",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const insertOrder = async (order) => {   ///TODO: Implement this
  
  // insert order into Orders table
  const {id, user_id, total_amount, products} = order

  // if we aren't given an id, make one.
  let newid = id

  if (!id){
    newid = uuid.v4()
  }

  const command = new PutCommand({
    TableName: "Orders",
    Item: {
      id: newid,
      user_id: user_id,
      products: products,
      total_amount: total_amount
    },
  });
  const response = await docClient.send(command);
  return response;
};

const updateUser = async (id, updates) => {  //TODO: Implement this

  const updateFields = [];
  const expressionAttributeValues = {};
  const expressionAttributeNames = {};

  if (updates.email) {
    updateFields.push("#email = :email");
    expressionAttributeValues[":email"] = updates.email;
    expressionAttributeNames["#email"] = "email";
  }
  if (updates.password) {
    updateFields.push("#password = :password");
    expressionAttributeValues[":password"] = updates.password;
    expressionAttributeNames["#password"] = "password";
  }
  if (updates.name) {
    updateFields.push("#name = :name");
    expressionAttributeValues[":name"] = updates.name;
    expressionAttributeNames["#name"] = "name"; 
  }

  const updateExpression = `set ${updateFields.join(", ")}`;

  const command = new UpdateCommand({
    TableName: "Users",
    Key: {
      id: id,
    },
    UpdateExpression: updateExpression,
    ExpressionAttributeValues: expressionAttributeValues,
    ExpressionAttributeNames: expressionAttributeNames,
    ReturnValues: "UPDATED_NEW",
  });

  const response = await docClient.send(command);
  return response;


};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
