const logger = require("./logger");

const db = process.argv[3];

const {
  init,
  queryRandomProduct,
  queryUserById,
  queryAllProducts,
  queryProductById,
  queryAllCategories,
  queryAllOrders,
  queryOrdersByUser,
  queryOrderById,
  queryAllUsers,
  insertOrder,
  updateUser,
} = require(db === "dynamo"
  ? "./dynamo_db.js"
  : db === "sql"
  ? "./mysql_db.js"
  : "");

  var numRequests = 0;

  function logReq(reqType, query){
    logger.info(`${reqType} request received for ${query}`);
    numRequests++;
    logger.info(`Total number of requests received = ${numRequests}`);
  }

const getRandomProduct = async (req, res) => {
  logReq("get", "randomProduct");
  const randProd = await queryRandomProduct();
  res.send(randProd);
};

const getProduct = async (req, res) => {
  const { productId } = req.params; // changed from params
  logReq("get", `product ID #${productId}`);
  const product = await queryProductById(productId);
  res.send(product);
};

const getProducts = async (req, res) => {
  const { category } = req.query;
  logReq("get", `all products in category ${category}`);
  const products = await queryAllProducts(category);
  res.send(products);
};

const getCategories = async (req, res) => {
  logReq("get", "allProducts");
  const categories = await queryAllCategories();
  res.send(categories);
};

const getAllOrders = async (req, res) => {
  logReq("get", "allOrders");
  const orders = await queryAllOrders();
  res.send(orders);
};

const getOrdersByUser = async (req, res) => {
  const { userId } = req.query;
  logReq("get", `orders for user #${userId}`);
  const orders = await queryOrdersByUser(userId);
  res.send(orders);
};

const getOrder = async (req, res) => {
  const { orderId } = req.params;
  logReq("get", `order ID #${orderId}`);
  const order = await queryOrderById(orderId);
  res.send(order);
};

const getUser = async (req, res) => {
  const { userId } = req.params;
  logReq("get", `user #${userId}`);
  const user = await queryUserById(userId);
  res.send(user);
};

const getUsers = async (req, res) => {
  logReq("get", "allUsers");
  const users = await queryAllUsers();
  res.send(users);
};

///TODO: Implement controller for POST /orders here
const postOrder = async (req, res) => {
  const orderObj = req.body;
  logReq("post", "order");
  const order = await insertOrder(orderObj);
  res.send(order); // Return successful order
  
};

const patchUser = async (req, res) => {
  const updates = req.body;
  const { userId } = req.params;
  logReq("patch", `patching user ID #${userId} with updates: ${updates}`);
  const response = await updateUser(userId, updates);
  res.send(response);
};

module.exports = {
  init,
  getProduct,
  getRandomProduct,
  getCategories,
  getAllOrders,
  getOrdersByUser,
  getOrder,
  getProducts,
  getUser,
  getUsers,
  postOrder, ///TODO: Export controller for POST /orders,
  patchUser,
};
